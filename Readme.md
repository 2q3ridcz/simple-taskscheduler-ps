# simple-taskscheduler

Runs the ps1 script when the time comes.

## Why not use window's task scheduer?

I would if I could, but my office won't let me. So I had to reinvent a cheep wheel...

## Usage

Check "./console_sample" for a runnable sample.

simple-taskscheduler consists of two scripts.
- [script name]_Scheduler.bat
- [script name]_Scheduler.ps1

To use simple-taskscheduler, you need the script you want to run (let's say "the-script.ps1" for now) and set its directory like this.

- (some directory)
    - Input
        - ScheduledTime.txt
    - the-script.ps1
    - the-script_Scheduler.bat
    - the-script_Scheduler.ps1

Rename the two scripts of simple-taskscheduler to match "the-script".

Write the time to run in "ScheduledTime.txt", and place it under "Input" folder. The time should be written in "hh:mm:ss" style.

Now you are ready to run "the-script_Scheduler.ps1". It will wait first, and when the time comes, it runs "the-script.ps1".

The same can be done by running "the-script_Scheduler.bat". If you place a shortcut of the bat file to the Startup folder, it will start automatically when the PC is on. For this usage, you must use the bat file because Startup doesn't start ps1 files.

Startup folder is here: C:\Users\[your user name]\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup

Just in case. If you want to stop simple-taskscheduler while it's running, hit Ctrl + C on the console.
