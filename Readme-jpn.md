# simple-taskscheduler

指定した時刻に ps1ファイルを実行します。

## 背景

Windows標準の TaskSchedulerを使えばいいじゃん…なのですが、オフィスのPCでは使用を制限されています。泣く泣く、貧弱な車輪を再発明することにしました。

## 使用方法

"./console_sample" に実行可能なサンプルがあります。そちらもご参考にしてください。

simple-taskscheduler は 2つのファイルで成り立っています。
- [スクリプト名]_Scheduler.bat
- [スクリプト名]_Scheduler.ps1

simple-taskscheduler を使用するには、実行したい ps1ファイル (ここでは "the-script.ps1" とします) を用意し、下記フォルダ構成のように配置します。

- (任意のフォルダ)
    - Input
        - ScheduledTime.txt
    - the-script.ps1
    - the-script_Scheduler.bat
    - the-script_Scheduler.ps1

simple-taskscheduler の 2つのファイルの名前は、実行したい ps1ファイルに合わせて変更してください。

そして、Inputフォルダに "ScheduledTime.txt" を置きます。このファイルには "hh:mm:ss" 形式で実行時刻を記入しておきます。

後は "the-script_Scheduler.ps1" を実行すれば待ちに入り、指定した時間に "the-script.ps1" を実行します。

"the-script_Scheduler.bat" を実行して同じことをすることも可能です。この batファイルのショートカットを Startup フォルダに配置することで、PC立上げ時に simple-taskscheduler を自動実行することができます。Startupは ps1ファイルを実行しないため、この場合は batファイルを使用してください。

Startup フォルダ：  C:\Users\[ユーザ名]\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup

simple-taskscheduler 実行後にキャンセルする場合は、コンソール上で Ctrl + C を押下してください。
