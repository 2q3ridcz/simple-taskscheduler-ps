@echo off

echo ## Path of this bat file
set SelfFilePath=%~0
echo SelfFilePath=%SelfFilePath%
echo.

echo ## Path of the ps1 file
set Ps1FilePath=%SelfFilePath:~0,-4%.ps1
echo Ps1FilePath =%Ps1FilePath%
echo.

echo ## Execute ps1 file
powershell -ExecutionPolicy RemoteSigned -File %Ps1FilePath%

pause
exit
