﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
Push-Location -Path $here

$InputFolderPath = "$here\Input" | Resolve-Path

### Get script file
$SelfFile = Get-ChildItem -Path $MyInvocation.MyCommand.Path
$ScriptBaseName = $SelfFile.BaseName.Substring(0, $SelfFile.BaseName.IndexOf("_Scheduler"))
$ScriptFilePath = $SelfFile.Directory | Join-Path -ChildPath ($ScriptBaseName + ".ps1")
$ScriptFile = Get-ChildItem -Path $ScriptFilePath
If ($Null -Eq $ScriptFile) { Throw ("Script does not exist: " + $ScriptFilePath) } 
"Script: " + $ScriptFile.FullName | %{(Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + "  " + $_} | Write-Host 


### Get trigger time
$TriggerTimeFilePath = $InputFolderPath | Join-Path -ChildPath "ScheduledTime.txt"
$TimeString = @(Get-Content -Path $TriggerTimeFilePath -Encoding UTF8)[0]
$TargetTime = Get-Date -Date $TimeString
If ($Null -Eq $TargetTime)  { Throw ("Failed to get target time.") } 

### Get sleep seconds
$Now = [datetime]::Now
If ( $TargetTime -Le $Now ) { $TargetTime = $TargetTime.AddDays(1) }
$Seconds = ($TargetTime - $Now).TotalSeconds

### Sleep and trigger
"Will be triggered around " + $TargetTime.ToString("yyyy/MM/dd HH:mm:ss") | %{(Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + "  " + $_} | Write-Host 
"Sleep for $([int]$Seconds) seconds..." | %{(Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + "  " + $_} | Write-Host 
Start-Sleep -Seconds $Seconds
"It's time! " | %{(Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + "  " + $_} | Write-Host 
&($ScriptFile.FullName)
